## v0.7.0 (2024-01-19)

### Feat

- add BinaryHeap::remove
- add overload for CppRO::incrementalShortestPathTree

### Fix

- change heap removal to an actual removal

### Refactor

- add insertInHeap in incrementalShortestPathTree
- add insertInHeap lambda in incrementalShortestPathTree

## v0.6.2 (2024-01-18)

### Fix

- add CTAD for IncrementalRelaxedEdge

## v0.6.1 (2024-01-18)

### Fix

- move order of handle and swap in BinaryHeap::pop

## v0.6.0 (2024-01-18)

### Feat

- add visitor calls in incrementalShortestPathTree

## 0.5.2 (2023-07-31)

### Fix

- Fix wrong color comparison

## 0.5.1 (2023-07-31)

### Fix

- Remove unncessary copy of Graph objects

## 0.5.0 (2023-07-31)

### Feat

- Added incremental algorithm for checing shortest-path uniqueness
- Added BinaryHeap::Handble default constructor

### Fix

- Removed CPLEX requirement for compilation
- BinaryHeap provides a handle_type member type
