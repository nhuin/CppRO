#include "CppRO/BinaryHeap.hpp"
#include <CppRO/ShortestPath.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_selectors.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/graph_utility.hpp>
#include <boost/graph/properties.hpp>
#include <boost/pending/property.hpp>
#include <catch2/catch_test_macros.hpp>
#include <limits>

// NOLINTBEGIN(*)

struct Link {
    std::size_t id;
};
using Graph = boost::adjacency_list<boost::vecS, boost::vecS,
    boost::bidirectionalS, boost::no_property, Link>;
using WeightType = std::size_t;
using edge_descriptor = typename boost::graph_traits<Graph>::edge_descriptor;
using HeapValue = std::pair<WeightType, Graph::vertex_descriptor>;
using HeapType = CppRO::BinaryHeap<HeapValue>;
using HeapHandle = HeapType::handle_type;

SCENARIO("Dijkstra's algorithm gives the shortest path") {
    GIVEN("A path of 2 nodes") {
        Graph g;
        const auto ed1 = add_edge(0, 1, {0}, g);
        const auto ed2 = add_edge(0, 1, {1}, g);
        std::vector<WeightType> weights{{1, 10}};
        const auto weightMap =
            boost::iterator_property_map(weights.begin(), get(&Link::id, g));

        std::vector<edge_descriptor> predecessors(num_vertices(g));
        const auto predMap = boost::iterator_property_map(
            predecessors.begin(), get(boost::vertex_index, g));
        std::vector<WeightType> distances(num_vertices(g));
        const auto distMap = boost::iterator_property_map(
            distances.begin(), get(boost::vertex_index, g));
        std::vector<boost::default_color_type> colors(num_vertices(g));
        const auto colorMap = boost::iterator_property_map(
            colors.begin(), get(boost::vertex_index, g));

        // Heap
        HeapType heap(num_vertices(g));
        std::vector<HeapHandle> handles(num_vertices(g));
        const auto handleMap = boost::iterator_property_map(
            handles.begin(), get(boost::vertex_index, g));

        WHEN("we check if the first edge can be relaxed") {
            CppRO::dijkstraInitialization(g, 0, distMap, colorMap, heap, 0,
                std::numeric_limits<WeightType>::max());
            const auto relaxStatus =
                CppRO::relaxEdge(g, ed1.first, colorMap, distMap, weightMap,
                    predMap, heap, handleMap, std::less<>(), std::plus<>());
            THEN("it relaxed it") {
                REQUIRE(relaxStatus == CppRO::EdgeStatus::Relaxed);
            }
        }
        WHEN("we check if the second edge can be relaxed after the first") {
            CppRO::dijkstraInitialization(g, 0, distMap, colorMap, heap, 0,
                std::numeric_limits<WeightType>::max());
            CppRO::relaxEdge(g, ed1.first, colorMap, distMap, weightMap,
                predMap, heap, handleMap, std::less<>(), std::plus<>());
            const auto relaxStatus =
                CppRO::relaxEdge(g, ed2.first, colorMap, distMap, weightMap,
                    predMap, heap, handleMap, std::less<>(), std::plus<>());
            THEN("it can't relaxed it") {
                REQUIRE(relaxStatus == CppRO::EdgeStatus::NotRelaxed);
            }
        }
        WHEN("we check if the first edge can be relaxed twice") {
            CppRO::dijkstraInitialization(g, 0, distMap, colorMap, heap, 0,
                std::numeric_limits<WeightType>::max());
            CppRO::relaxEdge(g, ed1.first, colorMap, distMap, weightMap,
                predMap, heap, handleMap, std::less<>(), std::plus<>());
            const auto relaxStatus =
                CppRO::relaxEdge(g, ed1.first, colorMap, distMap, weightMap,
                    predMap, heap, handleMap, std::less<>(), std::plus<>());
            THEN("it's already relaxed") {
                REQUIRE(relaxStatus == CppRO::EdgeStatus::AlreadyRelaxed);
            }
        }
        WHEN("we search the shortest path between 0 and 1") {
            CppRO::dijkstraShortestPath(g, 0, 1,
                boost::iterator_property_map(
                    predecessors.begin(), get(boost::vertex_index, g)),
                boost::iterator_property_map(
                    distances.begin(), get(boost::vertex_index, g)),
                boost::iterator_property_map(
                    weights.begin(), get(&Link::id, g)),
                heap,
                boost::iterator_property_map(
                    handles.begin(), get(boost::vertex_index, g)),
                std::less<>(), std::plus<>(), 0,
                std::numeric_limits<WeightType>::max(),
                boost::iterator_property_map(
                    colors.begin(), get(boost::vertex_index, g)));
            THEN("we find the path as the shortest path") {
                REQUIRE(source(predecessors[1], g) == 0);
                REQUIRE(distances[1] == 1);
                REQUIRE(distances[0] == 0);
                REQUIRE(
                    colors[0]
                    == boost::color_traits<boost::default_color_type>::black());
            }
        }
        WHEN("we add a new edge between 0 and 1 with a bigger weight") {
            weights.push_back(2);
            add_edge(0, 1, {1}, g);
            CppRO::dijkstraShortestPath(g, 0, 1,
                boost::iterator_property_map(
                    predecessors.begin(), get(boost::vertex_index, g)),
                boost::iterator_property_map(
                    distances.begin(), get(boost::vertex_index, g)),
                boost::iterator_property_map(
                    weights.begin(), get(&Link::id, g)),
                heap,
                boost::iterator_property_map(
                    handles.begin(), get(boost::vertex_index, g)),
                std::less<>(), std::plus<>(), 0,
                std::numeric_limits<WeightType>::max(),
                boost::iterator_property_map(
                    colors.begin(), get(boost::vertex_index, g)));
            THEN("we still use the same edge") {
                REQUIRE(distances[1] == 1);
                REQUIRE(distances[0] == 0);
                REQUIRE(
                    colors[0]
                    == boost::color_traits<boost::default_color_type>::black());
            }
        }
        WHEN("we add a new edge between 0 and 1 with a lower weight") {
            add_edge(0, 1, {weights.size()}, g);
            weights.push_back(0);

            CppRO::dijkstraShortestPath(g, 0, 1,
                boost::iterator_property_map(
                    predecessors.begin(), get(boost::vertex_index, g)),
                boost::iterator_property_map(
                    distances.begin(), get(boost::vertex_index, g)),
                boost::iterator_property_map(
                    weights.begin(), get(&Link::id, g)),
                heap,
                boost::iterator_property_map(
                    handles.begin(), get(boost::vertex_index, g)),
                std::less<>(), std::plus<>(), 0,
                std::numeric_limits<WeightType>::max(),
                boost::iterator_property_map(
                    colors.begin(), get(boost::vertex_index, g)));
            THEN("we use the new edge") {
                REQUIRE(distances[1] == 0);
                REQUIRE(distances[0] == 0);
                REQUIRE(
                    colors[0]
                    == boost::color_traits<boost::default_color_type>::black());
            }
        }
    }
}

SCENARIO("We can incrementaly update the shortest path tree of a graph") {
    GIVEN("A triangle graph and its shortest path tree") {
        Graph triangle;
        add_edge(0, 1, {0}, triangle);
        add_edge(0, 2, {1}, triangle);
        add_edge(1, 2, {2}, triangle);
        std::vector<edge_descriptor> predecessors(num_vertices(triangle));
        std::vector<WeightType> distances(num_vertices(triangle));
        predecessors[1] = edge(0, 1, triangle).first;
        predecessors[2] = edge(1, 2, triangle).first;
        distances[0] = 0;
        distances[1] = 1;
        distances[2] = 2;
        std::vector<WeightType> weights = {1, 3, 1};
        WHEN("w(0, 2) becomes 1") {
            weights[1] = 1;
            THEN("the shortest path tree is unique") {
                std::vector<boost::default_color_type> colors(
                    num_vertices(triangle));
                // Heap
                HeapType heap(num_vertices(triangle));
                std::vector<HeapHandle> handles(num_vertices(triangle));
                REQUIRE(CppRO::incrementalHasUniqueShortestPathTree(triangle,
                    std::vector{2ul},
                    boost::iterator_property_map(predecessors.begin(),
                        get(boost::vertex_index, triangle)),
                    boost::iterator_property_map(
                        distances.begin(), get(boost::vertex_index, triangle)),
                    boost::iterator_property_map(
                        weights.begin(), get(&Link::id, triangle)),
                    heap,
                    boost::iterator_property_map(
                        handles.begin(), get(boost::vertex_index, triangle)),
                    std::compare_three_way(), std::plus<>(),
                    std::numeric_limits<WeightType>::max(),
                    boost::iterator_property_map(
                        colors.begin(), get(boost::vertex_index, triangle))));
                REQUIRE(incident(predecessors[2], triangle)
                        == incident(edge(0, 2, triangle).first, triangle));
            }
            THEN("(0,2) is the new pred") {
                std::vector<boost::default_color_type> colors(
                    num_vertices(triangle));
                const auto [newDistance, ed] =
                    CppRO::getNewDistance(triangle, 2,
                        boost::iterator_property_map(
                            weights.begin(), get(&Link::id, triangle)),
                        boost::iterator_property_map(distances.begin(),
                            get(boost::vertex_index, triangle)),
                        std::compare_three_way(), std::plus<>(),
                        std::numeric_limits<WeightType>::max());
                REQUIRE(newDistance == 1);
                // Heap
                HeapType heap(num_vertices(triangle));
                std::vector<HeapHandle> handles(num_vertices(triangle));

                CppRO::incrementalShortestPathTree(triangle, std::vector{2ul},
                    boost::iterator_property_map(predecessors.begin(),
                        get(boost::vertex_index, triangle)),
                    boost::iterator_property_map(
                        distances.begin(), get(boost::vertex_index, triangle)),
                    boost::iterator_property_map(
                        weights.begin(), get(&Link::id, triangle)),
                    heap,
                    boost::iterator_property_map(
                        handles.begin(), get(boost::vertex_index, triangle)),
                    std::compare_three_way(), std::plus<>(),
                    std::numeric_limits<WeightType>::max(),
                    boost::iterator_property_map(
                        colors.begin(), get(boost::vertex_index, triangle)));
                REQUIRE(incident(predecessors[2], triangle)
                        == incident(edge(0, 2, triangle).first, triangle));
            }
        }
        WHEN("w(1,2) becomes 3") {
            weights[2] = 3;
            THEN("the shortest path tree is unique") {
                std::vector<boost::default_color_type> colors(
                    num_vertices(triangle));
                // Heap
                HeapType heap(num_vertices(triangle));
                std::vector<HeapHandle> handles(num_vertices(triangle));
                REQUIRE(CppRO::incrementalHasUniqueShortestPathTree(triangle,
                    std::vector{2ul},
                    boost::iterator_property_map(predecessors.begin(),
                        get(boost::vertex_index, triangle)),
                    boost::iterator_property_map(
                        distances.begin(), get(boost::vertex_index, triangle)),
                    boost::iterator_property_map(
                        weights.begin(), get(&Link::id, triangle)),
                    heap,
                    boost::iterator_property_map(
                        handles.begin(), get(boost::vertex_index, triangle)),
                    std::compare_three_way(), std::plus<>(),
                    std::numeric_limits<WeightType>::max(),
                    boost::iterator_property_map(
                        colors.begin(), get(boost::vertex_index, triangle))));
                REQUIRE(incident(predecessors[2], triangle)
                        == incident(edge(0, 2, triangle).first, triangle));
            }
            THEN("the new distance of 2 is 3") {
                const auto [newDistance, ed] =
                    CppRO::getNewDistance(triangle, 2,
                        boost::iterator_property_map(
                            weights.begin(), get(&Link::id, triangle)),
                        boost::iterator_property_map(distances.begin(),
                            get(boost::vertex_index, triangle)),
                        std::compare_three_way(), std::plus<>(),
                        std::numeric_limits<WeightType>::max());
                REQUIRE(newDistance == 3);
                REQUIRE(boost::incident(ed, triangle)
                        == boost::incident(
                            boost::edge(0, 2, triangle).first, triangle));
            }
            THEN("(0,2) is the new pred") {
                std::vector<boost::default_color_type> colors(
                    num_vertices(triangle));
                // Heap
                HeapType heap(num_vertices(triangle));
                std::vector<HeapHandle> handles(num_vertices(triangle));
                CppRO::incrementalShortestPathTree(triangle, std::vector{2ul},
                    boost::iterator_property_map(predecessors.begin(),
                        get(boost::vertex_index, triangle)),
                    boost::iterator_property_map(
                        distances.begin(), get(boost::vertex_index, triangle)),
                    boost::iterator_property_map(
                        weights.begin(), get(&Link::id, triangle)),
                    heap,
                    boost::iterator_property_map(
                        handles.begin(), get(boost::vertex_index, triangle)),
                    std::compare_three_way(), std::plus<>(),
                    std::numeric_limits<WeightType>::max(),
                    boost::iterator_property_map(
                        colors.begin(), get(boost::vertex_index, triangle)));
                REQUIRE(incident(predecessors[2], triangle)
                        == incident(edge(0, 2, triangle).first, triangle));
            }
        }
    }
}

// NOLINTEND(*)
