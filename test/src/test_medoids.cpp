#include <catch2/catch_test_macros.hpp>

#include <CppRO/KMedoids.hpp>
#include <vector>
#include <cstddef>

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)

SCENARIO("The k-medoids PAM heuristics", "[medoids]") {

    GIVEN("An instance") {
        const std::vector<double> capacities(10U, 10.0);
        const std::vector<double> charges(10U, 5.0);
        std::vector<std::vector<double>> distance(
            10U, std::vector<double>(10U, 1.0));
        for (std::size_t i = 0; i < distance.size(); ++i) {
            distance[i][i] = 0;
        }
        std::vector<std::size_t> medoids{1, 2, 3, 4, 5};

        WHEN("iterating 1 time") {
            const bool status =
                improveKMedoids(medoids, distance, capacities, charges, 1U);
            THEN("medoids change") { REQUIRE(status); }
        }
    }
}

// NOLINTEND(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
