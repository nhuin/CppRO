#include <CppRO/BinaryHeap.hpp>
#include <catch2/catch_test_macros.hpp>

// NOLINTBEGIN(*)

SCENARIO("A binary heap keeps object sorted") {
    GIVEN("A binary heap") {
        CppRO::BinaryHeap<int> heap(3);
        REQUIRE(heap.empty());
        heap.push(5);
        REQUIRE(heap.top() == 5);
        heap.push(4);
        REQUIRE(heap.top() == 4);
        heap.push(3);
        REQUIRE(heap.top() == 3);
        WHEN("we pop 3") {
            heap.pop();
            THEN("4 is the top") { REQUIRE(heap.top() == 4); }
            WHEN("we pop 4") {
                heap.pop();
                THEN("5 is the top") { REQUIRE(heap.top() == 5); }
                WHEN("we pop 5") {
                    heap.pop();
                    THEN("The heap is empty") { REQUIRE(heap.empty()); }
                }
            }
        }
    }
}

SCENARIO("A binary heap can be muted") {
    GIVEN("A binary heap") {
        CppRO::BinaryHeap<int> heap(3);
        std::vector<CppRO::BinaryHeap<int>::Handle> handles;
        REQUIRE(heap.empty());
        handles.emplace_back(heap.push(5));
        REQUIRE(heap.top() == 5);
        handles.emplace_back(heap.push(4));
        REQUIRE(heap.top() == 4);
        handles.emplace_back(heap.push(3));
        REQUIRE(heap.top() == 3);
        WHEN("we update 5 to 1") {
            heap.decrease(handles[0], 1);
            THEN("1 is the top") { REQUIRE(heap.top() == 1); }
        }
        WHEN("we pop 3 and update 5 to 1") {
            heap.pop();
            heap.decrease(handles[0], 1);
            THEN("1 is the top") { REQUIRE(heap.top() == 1); }
        }
    }
}

// NOLINTEND(*)
