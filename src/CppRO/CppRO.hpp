#ifndef CPPRO_HPP
#define CPPRO_HPP

#include <cassert>

// NOLINTBEGIN(cppcoreguidlines-macro-usage)
#ifndef DISABLE_CPPRO_ASSERT
#define CPPRO_ASSERT(x) assert(x)
#else
#define CPPRO_ASSERT(x) (void)0
#endif
// NOLINTEND(cppcoreguidlines-macro-usage)

#endif

