#ifndef BINARYHEAP_HPP
#define BINARYHEAP_HPP

#include "CppRO.hpp"
#include <algorithm>
#include <limits>
#include <vector>

namespace CppRO {
template <typename T, typename Comparator = std::less<T>>
class BinaryHeap {
  public:
    class Handle {
        explicit Handle(const std::size_t _i)
            : m_index(_i) {}
        std::size_t m_index{std::numeric_limits<std::size_t>::max()};
        friend BinaryHeap;

      public:
        Handle() = default;
    };

    using handle_type = Handle;

  private:
    struct Node {
        T object;
        std::size_t handling_index;
    };
    std::vector<Node> m_nodes;
    std::vector<std::size_t> m_handles;
    Comparator m_comparator;

  public:
    explicit BinaryHeap(
        std::size_t _maxNbElements, Comparator _comparator = Comparator())
        : m_comparator(std::move(_comparator)) {
        m_nodes.reserve(_maxNbElements);
        m_handles.reserve(_maxNbElements);
    }

    const T& top() const { return m_nodes[0].object; }

    [[nodiscard]] bool empty() const { return m_nodes.empty(); }

    /**
     * Given a object, push it into the heap and update the position of the
     * nodes
     */
    Handle push(T _object) {
        CPPRO_ASSERT(m_nodes.size() != m_nodes.capacity());

        m_nodes.emplace_back(Node{std::move(_object), m_handles.size()});
        m_handles.emplace_back(m_nodes.size() - 1);
        if (m_nodes.size() > 1) {
            std::size_t obj = m_nodes.size() - 1;
            std::size_t parent = (obj - 1) / 2;
            while (
                obj != 0
                && m_comparator(m_nodes[obj].object, m_nodes[parent].object)) {
                std::swap(m_nodes[obj], m_nodes[parent]);
                m_handles[m_nodes[obj].handling_index] = obj;
                m_handles[m_nodes[parent].handling_index] = parent;
                obj = parent;
                parent = (obj - 1) / 2;
            }
        }
        return Handle{m_handles.size() - 1};
    }

    void clear() {
        m_nodes.clear();
        m_handles.clear();
    }

    void pop() {
        m_handles[m_nodes[m_nodes.size() - 1].handling_index] = 0;
        std::swap(m_nodes[0], m_nodes[m_nodes.size() - 1]);
        m_nodes.pop_back();

        std::size_t obj = 0;
        std::size_t firstChild = 2 * obj + 1;
        std::size_t secondChild = firstChild + 1;
        while (firstChild < m_nodes.size()) {
            std::size_t child = secondChild < m_nodes.size()
                                    ? (m_comparator(m_nodes[firstChild].object,
                                           m_nodes[secondChild].object)
                                            ? firstChild
                                            : secondChild)
                                    : firstChild;
            if (m_comparator(m_nodes[child].object, m_nodes[obj].object)) {
                std::swap(m_nodes[child], m_nodes[obj]);
                m_handles[m_nodes[obj].handling_index] = obj;
                m_handles[m_nodes[child].handling_index] = child;
                obj = child;
                firstChild = 2 * obj + 1;
                secondChild = firstChild + 1;
            } else {
                break;
            }
        }
    }

    void decrease(Handle _h) {
        std::size_t obj = m_handles[_h.m_index];
        std::size_t parent = (obj - 1) / 2;
        while (obj != 0
               && m_comparator(m_nodes[obj].object, m_nodes[parent].object)) {
            std::swap(m_nodes[obj], m_nodes[parent]);
            m_handles[m_nodes[obj].handling_index] = obj;
            m_handles[m_nodes[parent].handling_index] = parent;
            obj = parent;
            parent = (obj - 1) / 2;
        }
    }

    void decrease(Handle _h, T _obj) {
        m_nodes[m_handles[_h.m_index]].object = std::move(_obj);
        decrease(_h);
    }

    void remove(Handle _h) {
        m_handles[m_nodes[m_nodes.size() - 1].handling_index] = _h.m_index;
        std::swap(m_nodes[_h.m_index], m_nodes[m_nodes.size() - 1]);
        m_nodes.pop_back();

        std::size_t obj = _h.m_index;
        std::size_t firstChild = 2 * obj + 1;
        std::size_t secondChild = firstChild + 1;
        while (firstChild < m_nodes.size()) {
            std::size_t child = secondChild < m_nodes.size()
                                    ? (m_comparator(m_nodes[firstChild].object,
                                           m_nodes[secondChild].object)
                                            ? firstChild
                                            : secondChild)
                                    : firstChild;
            if (m_comparator(m_nodes[child].object, m_nodes[obj].object)) {
                std::swap(m_nodes[child], m_nodes[obj]);
                m_handles[m_nodes[obj].handling_index] = obj;
                m_handles[m_nodes[child].handling_index] = child;
                obj = child;
                firstChild = 2 * obj + 1;
                secondChild = firstChild + 1;
            } else {
                break;
            }
        }
    }

    void increase(Handle _h) {
        auto obj = m_handles[_h.m_index];
        auto firstChild = 2 * obj + 1;
        auto secondChild = firstChild + 1;
        while (firstChild < m_nodes.size()) {
            // Swap with biggest child
            std::size_t child = secondChild < m_nodes.size()
                                    ? (m_comparator(m_nodes[firstChild].object,
                                           m_nodes[secondChild].object)
                                            ? firstChild
                                            : secondChild)
                                    : firstChild;
            if (m_comparator(m_nodes[child].object, m_nodes[obj].object)) {
                std::swap(m_nodes[child], m_nodes[obj]);
                m_handles[m_nodes[obj].handling_index] = obj;
                m_handles[m_nodes[child].handling_index] = child;
                obj = child;
                firstChild = 2 * obj + 1;
                secondChild = firstChild + 1;
            } else {
                break;
            }
        }
    }

    void increase(Handle _h, T _obj) {
        m_nodes[m_handles[_h.m_index]].object = std::move(_obj);
        increase(_h);
    }

    void update(Handle _h, T _obj) {
        if (m_comparator(m_nodes[m_handles[_h.m_index]].object, _obj)) {
            decrease(_h, std::move(_obj));
        } else {
            increase(_h, std::move(_obj));
        }
    }
};
} // namespace CppRO
#endif
