#ifndef KMEDOIDS_HPP
#define KMEDOIDS_HPP

#include <algorithm>
#include <limits>
#include <numeric>
#include <vector>

#include "Matrix.hpp"
#include "MyRandom.hpp"
#include "utility.hpp"

constexpr auto INVALID_MEDOID = std::numeric_limits<std::size_t>::max();

template <typename DistanceMap>
std::vector<std::size_t> getKMedoids(const std::size_t _k,
    const DistanceMap& _dist, const std::size_t _tMax = 100) {

    std::vector<std::vector<std::size_t>> clusters(_k);
    // Init medoids
    std::vector<std::size_t> medoids =
        MyRandom::getInstance().getKShuffled<std::size_t>(
            _k, 0u, _dist.shape()[1] - 1);
    bool changed = true;
    for (std::size_t t = 0; t < _tMax && changed; ++t) {
        changed = false;
        for (auto& vect : clusters) {
            vect.clear();
        }
        // For each elements, find the closest medoid and assign the element to
        // the cluster of the corresponding medoid
        for (std::size_t i = 0; i < _dist.shape()[0]; ++i) {
            std::size_t minInd = 0;
            for (std::size_t j = 1; j < medoids.size(); ++j) {
                if (_dist[i][medoids[j]] < _dist[i][medoids[minInd]]) {
                    minInd = j;
                }
            }
            clusters[minInd].push_back(i);
        }
        // Search medoid for each cluster
        for (std::size_t c = 0; c < clusters.size(); ++c) {
            // Compute distance between all elements of the cluster
            if (clusters[c].empty()) {
                if (medoids[c] != INVALID_MEDOID) {
                    medoids[c] = INVALID_MEDOID;
                    changed = true;
                }
                continue;
            }
            std::size_t minDistInd = 0;
            double minDist =
                std::accumulate(clusters[c].begin(), clusters[c].end(), 0.0,
                    [&](const double __tmpDist, const std::size_t __elj) {
                        return __tmpDist + _dist[clusters[c][0]][__elj];
                    });
            for (std::size_t i = 1; i < clusters[c].size(); ++i) {
                double distance = 0.0;
                for (std::size_t j = 0; j < clusters[c].size(); ++j) {
                    distance += _dist[clusters[c][i]][clusters[c][j]];
                    if (distance > minDist) {
                        break;
                    }
                }
                if (distance < minDist) {
                    minDistInd = i;
                }
            }
            if (medoids[c] != clusters[c][minDistInd]) {
                changed = true;
            }
            medoids[c] = clusters[c][minDistInd];
        }
    }
    return medoids;
}

template <typename DistanceMap>
[[deprecated]] std::vector<std::size_t> getKMedoids(const std::size_t _k,
    const DistanceMap& _dist, const std::vector<double>& _capas,
    const std::vector<double>& _charge, const std::size_t _tMax = 100) {
    assert(_capas.size() == _dist.shape()[0]);
    assert(_charge.size() == _dist.shape()[0]);
    assert(_dist.shape()[0] == _dist.shape()[1]);

    std::vector<std::vector<std::size_t>> clusters(_k);
    // Init medoids
    std::vector<std::size_t> medoids =
        MyRandom::getInstance().getKShuffled<std::size_t>(
            _k, 0, _dist.shape()[1] - 1);
    std::vector<double> usedCapas(_dist.shape()[1], 0.0);
    bool changed = true;
    for (std::size_t t = 0; t < _tMax && changed; ++t) {
        std::fill(usedCapas.begin(), usedCapas.end(), 0.0);
        changed = false;
        for (auto& vect : clusters) {
            vect.clear();
        }
        // For each element, find the closest medoid and assign the element to
        // the cluster of the corresponding medoid
        for (std::size_t i = 0; i < _dist.shape()[0]; ++i) {
            std::size_t minInd = 0;
            for (std::size_t j = 1; j < medoids.size(); ++j) {
                if (_dist[i][medoids[j]] < _dist[i][medoids[minInd]]
                    &&                          // Update closest medoid if
                                                // distance is smaller..
                    usedCapas[j] < _capas[j]) { // and still as available capa
                    minInd = j;
                }
            }
            clusters[minInd].push_back(i);
            usedCapas[minInd] -= _charge[i];
        }
        // Search medoid for each cluster
        for (std::size_t c = 0; c < clusters.size(); ++c) {
            // Compute distance between all elements of the cluster
            if (clusters[c].empty()) {
                if (medoids[c] != INVALID_MEDOID) {
                    medoids[c] = INVALID_MEDOID;
                    changed = true;
                }
                continue;
            }
            std::size_t minDistInd = 0;
            double minDist =
                std::accumulate(clusters[c].begin(), clusters[c].end(), 0.0,
                    [&](const double __tmpDist, const std::size_t __elj) {
                        return __tmpDist + _dist[clusters[c][0]][__elj];
                    });

            for (std::size_t i = 1; i < clusters[c].size(); ++i) {
                double distance = 0.0;
                for (std::size_t j = 0; j < clusters[c].size(); ++j) {
                    distance += _dist[clusters[c][i]][clusters[c][j]];
                    if (distance > minDist) {
                        break;
                    }
                }
                if (distance < minDist) {
                    minDistInd = i;
                }
            }

            if (medoids[c]
                != clusters[c][minDistInd]) { // Medoids of cluster c is changed
                changed = true;
                medoids[c] = clusters[c][minDistInd];
            }
        }
    }
    return medoids;
}

template <typename DistanceMatrix>
bool assignToCluster(const std::vector<std::size_t>& _medoids,
    std::vector<std::vector<std::size_t>>& _clusters,
    const DistanceMatrix& _dist, const std::vector<double>& _charge,
    const std::vector<double>& _capas, std::vector<double>& _usedCapas) {
    std::fill(_usedCapas.begin(), _usedCapas.end(), 0.0);
    for (auto& vect : _clusters) {
        vect.clear();
    }

    // For each element, find the closest medoid and assign the element to
    // the cluster of the corresponding medoid if there is enough capacity
    for (std::size_t element = 0; element < _charge.size(); ++element) {
        const auto isMedoidFree = [&](const auto _medoid) {
            return epsilon_less_equal<double>()(
                _usedCapas[_medoid] + _charge[element], _capas[_medoid]);
        };
        const auto ite = std::min_element(
            std::find_if(_medoids.begin(), _medoids.end(), isMedoidFree),
            _medoids.end(), [&](const auto _m1, const auto _m2) {
                return isMedoidFree(_m2)
                       && epsilon_less_equal<double>()(
                           _dist[_m1][element], _dist[_m2][element]);
            });
        if (ite == _medoids.end()) {
            // No medoid available due to capacity constraints
            return false;
        }
        const auto c =
            static_cast<std::size_t>(std::distance(_medoids.begin(), ite));
        _clusters[c].push_back(element);
        _usedCapas[c] += _charge[element];
    }
    return true;
}

template <typename DistanceMatrix>
bool updateMedoids(std::vector<std::size_t>& _medoids,
    const std::vector<std::vector<std::size_t>>& _clusters,
    const DistanceMatrix& _dist, const std::vector<double>& _capas,
    std::vector<double>& _usedCapas) {
    bool changed = false;
    for (std::size_t c = 0; c < _clusters.size(); ++c) {
        assert(!_clusters[c].empty());
        // Given an element of a cluser, compute the sum of all distance
        // to the other element of the cluster
        const auto distanceToAll = [&](const std::vector<std::size_t>& _cluster,
                                       const std::size_t _element) {
            assert(_dist[_element][_element] == 0.0);
            return std::accumulate(_cluster.begin(), _cluster.end(), 0.0,
                [&](const auto _acc, const auto _otherElement) {
                    return _acc + _dist[_element][_otherElement];
                });
        };
        // Given a new possible medoid, check that it has enough
        // capacity
        const auto isNewMedoidFree = [&](const auto _newMedoid) {
            return epsilon_less_equal<double>()(
                _usedCapas[_medoids[c]], _capas[_newMedoid]);
        };
        // Search for element with shortest distance with everyone else
        const auto currentMedoidInformation = std::make_pair(
            _medoids[c], distanceToAll(_clusters[c], _medoids[c]));
        const auto newMedoid = std::accumulate(_clusters[c].begin(),
            _clusters[c].end(), currentMedoidInformation,
            [&](auto _bestElement, std::size_t _testElement) {
                if (!isNewMedoidFree(_testElement)) {
                    return _bestElement;
                }
                const auto dist = distanceToAll(_clusters[c], _testElement);
                if (dist < _bestElement.second) {
                    return std::make_pair(_testElement, dist);
                }
                return _bestElement;
            });
        // Check if improvement and update accordingly
        if (_medoids[c] != newMedoid.first) {
            changed = true;
            _medoids[c] = newMedoid.first;
        }
    }
    return changed;
}

/**
 * Given a selection of medoids, a charge for each elements and a capacity for
 * each location, iteratively improves the medoids until convergence or the
 * maximum number of iterations is reached.
 */
template <typename DistanceMap>
bool improveKMedoids(std::vector<std::size_t>& _medoids,
    const DistanceMap& _dist, const std::vector<double>& _capas,
    const std::vector<double>& _charge, const std::size_t _tMax = 100) {

    std::vector<std::vector<std::size_t>> clusters(_medoids.size());
    std::vector<double> usedCapas(_capas.size(), 0.0);
    for (std::size_t t = 0; t < _tMax; ++t) {
        std::sort(_medoids.begin(), _medoids.end(),
            [&](const auto _m1, const auto _m2) {
                return epsilon_greater<double>()(_capas[_m1], _capas[_m2]);
            });
        if (!assignToCluster(
                _medoids, clusters, _dist, _charge, _capas, usedCapas)) {
            return false;
        }
        if (!updateMedoids(_medoids, clusters, _dist, _charge, usedCapas)) {
            return true;
        }
    }
    return true;
}

#endif
