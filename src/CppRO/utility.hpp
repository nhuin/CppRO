#ifndef UTILITY_HPP
#define UTILITY_HPP

#include <array>
#include <chrono>
#include <deque>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <tuple>
#include <vector>

#if defined(NDEBUG) || defined(PROFILE)
#define DBOUT(x)
#else
#define DBOUT(x) x
#endif


template <typename T>
int getMaxSetBit(T _v) {
    int ret = 0;
    while (_v >>= 1) {
        ++ret;
    }
    return ret;
}

template <typename T>
int getMaxSetByte(T _v) {
    int ret = 0;
    while (_v >>= 8) {
        ++ret;
    }
    return ret;
}

template <typename T>
T binomialCoeff(const T& _n, T _k) {
    T res = 1;
    // Since C(n, k) = C(n, n-k)
    if (_k > _n - _k) {
        _k = _n - _k;
    }

    for (T i = 0; i < _k; ++i) {
        res *= (_n - i);
        res /= (i + 1);
    }
    return res;
}

/**
 * for each for tuple
 */

template <typename Tuple, typename F, std::size_t... Indices>
constexpr void forEachImpl(
    Tuple&& _tuple, F&& _f, std::index_sequence<Indices...> /*unused*/) {
    (_f(std::get<Indices>(std::forward<Tuple>(_tuple))), ...);
}

template <typename Tuple, typename F>
constexpr void forEach(Tuple&& _tuple, F&& _f) {
    constexpr const auto N =
        std::tuple_size<std::remove_reference_t<Tuple>>::value;
    forEachImpl(std::forward<Tuple>(_tuple), std::forward<F>(_f),
        std::make_index_sequence<N>{});
}

template <typename Function>
struct CallOnDestroy {
    explicit CallOnDestroy(Function _f)
        : f(_f) {}
    CallOnDestroy(const CallOnDestroy&) = default;
    CallOnDestroy(CallOnDestroy&&) noexcept = default;
    CallOnDestroy& operator=(const CallOnDestroy&) = default;
    CallOnDestroy& operator=(CallOnDestroy&&) noexcept = default;
    ~CallOnDestroy() { f(); }
    Function f;
};

constexpr double DEFAULT_EPSILON = 1e-6;

template <typename T>
struct epsilon_less {
    explicit constexpr epsilon_less(const T& _value)
        : epsilon_value(_value) {}

    constexpr epsilon_less() = default;
    ~epsilon_less() = default;

    constexpr bool operator()(const T& _lhs, const T& _rhs) const {
        return _lhs + epsilon_value < _rhs;
    }
    T epsilon_value{DEFAULT_EPSILON};
};

template <typename T>
struct epsilon_less_equal {
    explicit constexpr epsilon_less_equal(const T& _value)
        : epsilon_value(_value) {}

    constexpr epsilon_less_equal() = default;
    ~epsilon_less_equal() = default;

    constexpr bool operator()(const T& _lhs, const T& _rhs) const {
        return _lhs + epsilon_value < _rhs
               || std::fabs(_lhs - _rhs) < epsilon_value;
    }
    T epsilon_value{DEFAULT_EPSILON};
};

template <typename T>
struct epsilon_greater {
    explicit constexpr epsilon_greater(const T& _value)
        : epsilon_value(_value) {}

    constexpr epsilon_greater() = default;
    ~epsilon_greater() = default;

    constexpr bool operator()(const T& _lhs, const T& _rhs) const {
        return _lhs - _rhs > epsilon_value;
    }
    T epsilon_value{DEFAULT_EPSILON};
};

template <typename T>
struct epsilon_greater_equal {
    explicit constexpr epsilon_greater_equal(const T& _value)
        : epsilon_value(_value) {}

    constexpr epsilon_greater_equal() = default;
    ~epsilon_greater_equal() = default;

    constexpr bool operator()(const T& _lhs, const T& _rhs) const {
        return _lhs + epsilon_value > _rhs
               || std::fabs(_lhs - _rhs) < epsilon_value;
    }
    T epsilon_value{DEFAULT_EPSILON};
};

template <typename T>
struct epsilon_equal {
    explicit constexpr epsilon_equal(const T& _value)
        : epsilon_value(_value) {}

    constexpr epsilon_equal() = default;
    ~epsilon_equal() = default;

    constexpr bool operator()(const T& _lhs, const T& _rhs) const {
        return std::fabs(_lhs - _rhs) < epsilon_value;
    }
    T epsilon_value{DEFAULT_EPSILON};
};

#endif
