#ifndef SHORTESTPATH_HPP
#define SHORTESTPATH_HPP

#include "CppRO.hpp"

#include <boost/graph/graph_traits.hpp>
#include <boost/graph/two_bit_color_map.hpp>
#include <boost/graph/visitors.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/range/iterator_range_core.hpp>
#include <cassert>
#include <concepts>
#include <functional>
#include <limits>
#include <numeric>
#include <type_traits>

namespace CppRO {

template <typename Map, typename KeyType, typename ValueType>
concept ReadablePropertyMap = requires(Map _a, KeyType _k) {
    { get(_a, _k) } -> std::convertible_to<ValueType>;
};

template <typename Map, typename KeyType, typename ValueType>
concept WritablePropertyMap = requires(Map _a, KeyType _k, ValueType _v) {
    { put(_a, _k, _v) };
};

template <typename Map, typename KeyType, typename ValueType>
concept ReadWritePropertyMap = requires(Map _a, KeyType _k) {
    ReadablePropertyMap<Map, KeyType, ValueType>;
    WritablePropertyMap<Map, KeyType, ValueType>;
};

template <typename Map, typename Graph, typename ValueType>
concept ReadableEdgePropertyMap = requires {
    ReadablePropertyMap<Map,
        typename boost::graph_traits<Graph>::edge_descriptor, ValueType>;
};

template <typename Map, typename Graph, typename ValueType>
concept WritableEdgePropertyMap = requires {
    WritablePropertyMap<Map,
        typename boost::graph_traits<Graph>::edge_descriptor, ValueType>;
};

template <typename Map, typename Graph, typename ValueType>
concept ReadWriteEdgePropertyMap = requires {
    ReadWritePropertyMap<Map,
        typename boost::graph_traits<Graph>::edge_descriptor, ValueType>;
};

template <typename Map, typename Graph, typename ValueType>
concept ReadableVertexPropertyMap = requires {
    ReadablePropertyMap<Map,
        typename boost::graph_traits<Graph>::vertex_descriptor, ValueType>;
};

template <typename Map, typename Graph, typename ValueType>
concept WritableVertexPropertyMap = requires {
    WritablePropertyMap<Map,
        typename boost::graph_traits<Graph>::vertex_descriptor, ValueType>;
};

template <typename Map, typename Graph, typename ValueType>
concept ReadWriteVertexPropertyMap = requires {
    ReadWritePropertyMap<Map,
        typename boost::graph_traits<Graph>::vertex_descriptor, ValueType>;
};

template <typename Map, typename KeyType, typename ValueType>
concept OnlyReadablePropertyMap = requires(Map _a, KeyType _k) {
    { get(_a, _k) } -> std::convertible_to<ValueType>;
    !WritablePropertyMap<Map, KeyType, ValueType>;
};

template <typename Graph, typename DistanceMap, typename HeapType,
    typename DistInf, typename DistZero, typename ColorMap>
void dijkstraInitialization(const Graph& _graph,
    typename boost::graph_traits<Graph>::vertex_descriptor _s,
    DistanceMap&& _distance, ColorMap&& _colors, HeapType&& _queue,
    DistZero&& _zero, DistInf&& _inf) {
    using ColorValue = typename boost::property_traits<
        std::remove_cvref_t<ColorMap>>::value_type;
    using Color = boost::color_traits<ColorValue>;
    for (auto u : boost::make_iterator_range(vertices(_graph))) {
        put(_distance, u, _inf);
        put(_colors, u, Color::white());
    }
    put(_distance, _s, _zero);
    put(_distance, _s, _zero);
    _queue.clear();
    _queue.push({_zero, _s});
    put(_colors, _s, Color::gray());
}

enum EdgeStatus { Relaxed, AlreadyRelaxed, NotRelaxed };

/**
 * Given an edge (u, v) of a graph, check if the current distance to v is
 * greater than the distance using (u, v).
 *
 *
 */
template <typename Graph, typename ColorMap, typename DistanceMap,
    typename WeightMap,
    ReadWriteVertexPropertyMap<Graph,
        typename boost::graph_traits<Graph>::vertex_iterator>
        PredecessorMap,
    typename HeapType, typename HeapHandleType, typename CompareFunction,
    typename CombineFunction>

    requires std::invocable<CompareFunction,
                 typename boost::property_traits<WeightMap>::value_type,
                 typename boost::property_traits<WeightMap>::value_type>
             && std::invocable<CombineFunction,
                 typename boost::property_traits<WeightMap>::value_type,
                 typename boost::property_traits<WeightMap>::value_type>

EdgeStatus relaxEdge(const Graph& _graph,
    typename boost::graph_traits<Graph>::edge_descriptor _ed,
    const ColorMap& _colors, DistanceMap&& _distance, const WeightMap& _weight,
    PredecessorMap&& _predecessors, HeapType&& _queue,
    HeapHandleType&& _handles, CompareFunction&& _compare,
    CombineFunction&& _combine) {

    using ColorValue = typename boost::property_traits<
        std::remove_cvref_t<ColorMap>>::value_type;
    using Color = boost::color_traits<ColorValue>;

    auto v = target(_ed, _graph);
    const auto color = get(_colors, v);
    const auto distUV =
        _combine(get(_distance, source(_ed, _graph)), get(_weight, _ed));
    if (_compare(distUV, get(_distance, v))) {
        put(_predecessors, v, _ed);
        put(_distance, v, distUV);
        if (color == Color::gray()) {
            _queue.decrease(get(_handles, v), {distUV, v});
        } else {
            put(_handles, v, _queue.push({distUV, v}));
            put(_colors, v, Color::gray());
        }
        return EdgeStatus::Relaxed;
    } else if (_compare(get(_distance, v), distUV)) {
        return EdgeStatus::NotRelaxed;
    }
    return EdgeStatus::AlreadyRelaxed;
}

struct BaseDijkstraVisitor {
    template <typename... Args>
    auto operator()(Args&&... /*args*/) const {}
};

template <typename Graph, typename PredecessorMap, typename WeightMap,
    ReadWriteVertexPropertyMap<Graph,
        typename boost::property_traits<WeightMap>::value_type>
        DistanceMap,
    typename HeapType, typename HandlePropertyMap, typename CompareFunction,
    typename CombineFunction, typename DistInf, typename DistZero,
    typename ColorMap, typename DijkstraVisitor = BaseDijkstraVisitor,
    typename WeightType = typename WeightMap::value_type>
void dijkstraShortestPath(const Graph& _graph,
    typename boost::graph_traits<Graph>::vertex_descriptor _s,
    typename boost::graph_traits<Graph>::vertex_descriptor _t,
    PredecessorMap&& _predecessors, DistanceMap&& _distance,
    const WeightMap& _weight, HeapType&& _queue, HandlePropertyMap&& _handles,
    CompareFunction&& _compare, CombineFunction&& _combine, DistZero&& _zero,
    DistInf&& _inf, ColorMap&& _colors,
    DijkstraVisitor&& _vis = DijkstraVisitor{})
    requires ReadWriteVertexPropertyMap<DistanceMap, Graph, WeightType>
             && ReadWriteVertexPropertyMap<PredecessorMap, Graph,
                 typename boost::graph_traits<Graph>::edge_descriptor>
             && std::invocable<CompareFunction, WeightType, WeightType>
{
    CPPRO_ASSERT(_compare(_zero, _inf));
    static_assert(
        std::is_invocable_r_v<bool, CompareFunction, WeightType, WeightType>);
    static_assert(std::is_invocable_r_v<WeightType, CombineFunction, WeightType,
        WeightType>);

    using ColorValue = typename boost::property_traits<ColorMap>::value_type;
    using Color = boost::color_traits<ColorValue>;

    dijkstraInitialization(_graph, _s, _distance, _colors, _queue, _zero, _inf);
    while (!_queue.empty() && _queue.top().second != _t) {
        auto u = _queue.top().second;
        _queue.pop();
        for (const auto ed : boost::make_iterator_range(out_edges(u, _graph))) {
            if (relaxEdge(_graph, ed, _colors, _distance, _weight,
                    _predecessors, _queue, _handles, _compare, _combine)
                == EdgeStatus::Relaxed) {
                _vis(ed, _graph, boost::on_edge_relaxed{});
            }
        }
        put(_colors, u, Color::black());
    }
}

/**
 * Given a graph and a source node, computes the shortest path tree with the
 * source node as the root using dijkstra algorithm
 */
template <typename Graph, typename PredecessorMap, typename WeightMap,
    ReadWriteVertexPropertyMap<Graph,
        typename boost::property_traits<WeightMap>::value_type>
        DistanceMap,
    typename HeapType, typename HandlePropertyMap, typename CompareFunction,
    typename CombineFunction, typename DistInf, typename DistZero,
    typename ColorMap, typename DijkstraVisitor = BaseDijkstraVisitor,
    typename WeightType = typename WeightMap::value_type>
void dijkstraShortestPath(const Graph& _graph,
    typename boost::graph_traits<Graph>::vertex_descriptor _s,
    PredecessorMap&& _predecessors, DistanceMap&& _distance,
    const WeightMap& _weight, HeapType&& _queue, HandlePropertyMap&& _handles,
    CompareFunction&& _compare, CombineFunction&& _combine, DistZero&& _zero,
    DistInf&& _inf, ColorMap&& _colors,
    DijkstraVisitor&& _vis = DijkstraVisitor{})
    requires ReadWriteVertexPropertyMap<DistanceMap, Graph, WeightType>
             && ReadWriteVertexPropertyMap<PredecessorMap, Graph,
                 typename boost::graph_traits<Graph>::edge_descriptor>
             && std::invocable<CompareFunction, WeightType, WeightType>
{
    using ColorValue = typename boost::property_traits<ColorMap>::value_type;
    using Color = boost::color_traits<ColorValue>;

    dijkstraInitialization(_graph, _s, _distance, _colors, _queue, _zero, _inf);
    while (!_queue.empty()) {
        auto u = _queue.top().second;
        put(_colors, u, Color::black());
        _queue.pop();
        for (const auto ed : boost::make_iterator_range(out_edges(u, _graph))) {
            if (relaxEdge(_graph, ed, _colors, _distance, _weight,
                    _predecessors, _queue, _handles, _compare, _combine)
                == EdgeStatus::Relaxed) {
                _vis(ed, _graph, boost::on_edge_relaxed{});
            }
        }
    }
}

/**
 * Given a graph, a source node, and a set of weigts, tests if the resulting
 * shortest path tree is unique.
 **/
template <typename Graph, typename PredecessorMap, typename WeightMap,
    ReadWriteVertexPropertyMap<Graph,
        typename boost::property_traits<WeightMap>::value_type>
        DistanceMap,
    typename HeapType, typename HandlePropertyMap, typename CompareFunction,
    typename CombineFunction, typename DistInf, typename DistZero,
    typename ColorMap, typename WeightType = typename WeightMap::value_type>
bool hasUniqueShortestPathTree(const Graph& _graph,
    typename boost::graph_traits<Graph>::vertex_descriptor _s,
    PredecessorMap&& _predecessors, DistanceMap&& _distance,
    const WeightMap& _weight, HeapType&& _queue, HandlePropertyMap&& _handles,
    CompareFunction&& _compare, CombineFunction&& _combine, DistZero&& _zero,
    DistInf&& _inf, ColorMap&& _colors)
    requires ReadWriteVertexPropertyMap<PredecessorMap, Graph,
                 typename boost::graph_traits<Graph>::edge_descriptor>
             && std::invocable<CompareFunction, WeightType, WeightType>
{
    using ColorValue = typename boost::property_traits<
        std::remove_cvref_t<ColorMap>>::value_type;
    using Color = boost::color_traits<ColorValue>;

    dijkstraInitialization(_graph, _s, _distance, _colors, _queue, _zero, _inf);
    while (!_queue.empty()) {
        auto u = _queue.top().second;
        put(_colors, u, Color::black());
        _queue.pop();
        if (std::any_of(out_edges(u, _graph).first, out_edges(u, _graph).second,
                [&](const auto _ed) {
                    return relaxEdge(_graph, _ed, _colors, _distance, _weight,
                               _predecessors, _queue, _handles, _compare,
                               _combine)
                           == EdgeStatus::AlreadyRelaxed;
                })) {
            return false;
        }
    }
    return true;
}

template <typename Graph, typename WeightMap, typename DistanceMap,
    typename CompareFunction, typename CombineFunction, typename DistInf>
auto computeUpdatedDistance(const Graph& _graph,
    typename boost::graph_traits<Graph>::vertex_descriptor _u,
    const WeightMap& _weight, const DistanceMap& _distance,
    CompareFunction&& _compare, CombineFunction&& _combine, DistInf _inf) {
    using edge_descriptor =
        typename boost::graph_traits<Graph>::edge_descriptor;
    const auto init = std::tuple{_inf, edge_descriptor{}, 0};
    return std::accumulate(in_edges(_u, _graph).first,
        in_edges(_u, _graph).second, init,
        [&](const auto _acc, const auto _ed) {
            const auto newDistance = _combine(
                get(_distance, source(_ed, _graph)), get(_weight, _ed));
            const auto comparison = _compare(newDistance, std::get<0>(_acc));
            if (comparison < 0) {
                return std::tuple{newDistance, _ed, 1};
            }
            if (comparison == 0) {
                return std::tuple{newDistance, _ed, 1 + std::get<2>(_acc)};
            }
            return _acc;
        });
}

/**
 * Given a graph and a vertex, return the smallest distance from the incoming
 * edges.
 */
template <typename Graph, typename WeightMap, typename DistanceMap,
    typename CompareFunction, typename CombineFunction, typename DistInf>
auto getNewDistance(const Graph& _graph,
    typename boost::graph_traits<Graph>::vertex_descriptor _u,
    const WeightMap& _weight, const DistanceMap& _distance,
    CompareFunction&& _compare, CombineFunction&& _combine, DistInf&& _inf) {
    const auto init =
        std::pair{_inf, typename boost::graph_traits<Graph>::edge_descriptor{}};
    return std::accumulate(in_edges(_u, _graph).first,
        in_edges(_u, _graph).second, init,
        [&](const auto _acc, const auto _ed) {
            const auto newDistance = _combine(
                get(_distance, source(_ed, _graph)), get(_weight, _ed));
            if (_compare(newDistance, _acc.first) < 0) {
                return std::pair{newDistance, _ed};
            }
            return _acc;
        });
}

template <typename VertexDescriptor, typename Distance, typename EdgeDescriptor>
struct IncrementalRelaxedEdge {
    VertexDescriptor u;
    std::pair<Distance, EdgeDescriptor> newDistance;
};

template <typename VertexDescriptor, typename Distance, typename EdgeDescriptor>
IncrementalRelaxedEdge(
    VertexDescriptor _vd, std::pair<Distance, EdgeDescriptor> _p)
    -> IncrementalRelaxedEdge<VertexDescriptor, Distance, EdgeDescriptor>;

struct NullVisitor {
    template <typename... Args>
    void operator()(Args... /*unused*/) const {}
};

template <typename Graph, typename ChangedVerticesCollection,
    typename PredecessorMap, typename WeightMap,
    ReadWriteVertexPropertyMap<Graph,
        typename boost::property_traits<WeightMap>::value_type>
        DistanceMap,
    typename HeapType, typename HandlePropertyMap, typename CompareFunction,
    typename CombineFunction, typename DistInf, typename ColorMap,
    typename WeightType = typename WeightMap::value_type,
    typename VisitorType = NullVisitor>
void incrementalShortestPathTree(const Graph& _graph,
    const ChangedVerticesCollection& _changedVertices,
    PredecessorMap&& _predecessors, DistanceMap&& _distance,
    const WeightMap& _weight, HeapType&& _queue, HandlePropertyMap&& _handles,
    CompareFunction&& _compare, CombineFunction&& _combine, DistInf&& _inf,
    ColorMap&& _colors, VisitorType&& _visitor = NullVisitor{})

    requires ReadWriteVertexPropertyMap<DistanceMap, Graph, WeightType>
             && ReadWriteVertexPropertyMap<PredecessorMap, Graph,
                 typename boost::graph_traits<Graph>::edge_descriptor>
             && std::invocable<CompareFunction, WeightType, WeightType>
{
    CPPRO_ASSERT(_queue.empty());

    using ColorValue = typename boost::property_traits<ColorMap>::value_type;
    using Color = boost::color_traits<ColorValue>;

    const auto insertInHeap = [&](const auto _u, auto _newDistance) {
        put(_handles, _u, _queue.push({_newDistance, _u}));
        put(_colors, _u, Color::gray());
    };

    // Given a node and its new distance, update the distance or add it to the
    // heap if it's not present
    const auto adjustHeap = [&](const auto _u, auto _newDistance) {
        const auto& color = get(_colors, _u);
        CPPRO_ASSERT(color != Color::black());
        if (color == Color::gray()) {
            _queue.update(get(_handles, _u), {_newDistance, _u});
        } else if (color == Color::white()) {
            insertInHeap(_u, _newDistance);
        }
    };

    const auto relaxNeighbors = [&](typename Graph::vertex_descriptor _v) {
        const auto newDistanceV = getNewDistance(
            _graph, _v, _weight, _distance, _compare, _combine, _inf);
        const auto comparison =
            _compare(newDistanceV.first, get(_distance, _v));
        if (comparison < 0) {
            adjustHeap(_v, newDistanceV.first);
        } else if (comparison > 0) {
            adjustHeap(_v, get(_distance, _v));
        } else if (get(_colors, _v) == Color::gray()) {
            _queue.remove(_handles[_v]);
            put(_colors, _v, Color::white());
        }
    };

    // A changed vertice can either be:
    // under-consistent (the new distance is greater than the current one) or,
    // over-consistent (the new distance is lesser than the current one)
    for (const auto u : _changedVertices) {
        const auto [newDistance, ed] = getNewDistance(
            _graph, u, _weight, _distance, _compare, _combine, _inf);
        const auto comparison = _compare(newDistance, get(_distance, u));
        CPPRO_ASSERT(comparison != 0);
        if (comparison < 0) {
            insertInHeap(u, newDistance);
        } else if (comparison > 0) {
            insertInHeap(u, get(_distance, u));
        }
    }
    while (!_queue.empty()) {
        auto u = _queue.top().second;
        _queue.pop();
        put(_colors, u, Color::white());
        const auto newDistance = getNewDistance(
            _graph, u, _weight, _distance, _compare, _combine, _inf);
        const auto comparison = _compare(newDistance.first, get(_distance, u));
        if (comparison < 0) {
            put(_distance, u, newDistance.first);
            put(_predecessors, u, newDistance.second);
            _visitor(IncrementalRelaxedEdge{u, newDistance});
            for (const auto ved :
                boost::make_iterator_range(out_edges(u, _graph))) {
                relaxNeighbors(target(ved, _graph));
            }
        } else if (comparison > 0) {
            put(_distance, u, _inf);
            relaxNeighbors(u);
            for (const auto ved :
                boost::make_iterator_range(out_edges(u, _graph))) {
                relaxNeighbors(target(ved, _graph));
            }
        }
    }
}

template <typename Graph, typename ChangedVerticesCollection,
    typename PredecessorMap, typename WeightMap, typename DistanceMap,
    typename HeapType, typename HandlePropertyMap, typename CompareFunction,
    typename CombineFunction, typename DistInf, typename ColorMap,
    typename WeightType = typename WeightMap::value_type>
bool incrementalHasUniqueShortestPathTree(Graph&& _graph,
    ChangedVerticesCollection&& _changedVertices,
    PredecessorMap&& _predecessors, DistanceMap&& _distance,
    WeightMap&& _weight, HeapType&& _queue, HandlePropertyMap&& _handles) {
    std::vector<boost::default_color_type> colors(num_vertices(_graph));
    const auto compare = std::compare_three_way();
    const auto combine = std::plus<>();
    const auto distInf = std::numeric_limits<WeightType>::max();
    return incrementalHasUniqueShortestPathTree(std::forward<Graph>(_graph),
        std::forward<ChangedVerticesCollection>(_changedVertices),
        std::forward<PredecessorMap>(_predecessors),
        std::forward<DistanceMap>(_distance), std::forward<WeightMap>(_weight),
        std::forward<HeapType>(_queue),
        std::forward<HandlePropertyMap>(_handles), compare, combine, distInf,
        colors);
}

template <typename Graph, typename ChangedVerticesCollection,
    typename PredecessorMap, typename WeightMap,
    ReadWriteVertexPropertyMap<Graph,
        typename boost::property_traits<WeightMap>::value_type>
        DistanceMap,
    typename HeapType, typename HandlePropertyMap, typename CompareFunction,
    typename CombineFunction, typename DistInf, typename ColorMap,
    typename WeightType = typename WeightMap::value_type>
bool incrementalHasUniqueShortestPathTree(const Graph& _graph,
    const ChangedVerticesCollection& _changedVertices,
    PredecessorMap&& _predecessors, DistanceMap&& _distance,
    const WeightMap& _weight, HeapType&& _queue, HandlePropertyMap&& _handles,
    CompareFunction&& _compare, CombineFunction&& _combine, DistInf&& _inf,
    ColorMap&& _colors)
    requires ReadWriteVertexPropertyMap<DistanceMap, Graph, WeightType>
             && ReadWriteVertexPropertyMap<PredecessorMap, Graph,
                 typename boost::graph_traits<Graph>::edge_descriptor>
             && std::invocable<CompareFunction, WeightType, WeightType>
{

    CPPRO_ASSERT(_queue.empty());

    using ColorValue = typename boost::property_traits<ColorMap>::value_type;
    using Color = boost::color_traits<ColorValue>;

    const auto insertInHeap = [&](const auto _u, auto _newDistance) {
        put(_handles, _u, _queue.push({_newDistance, _u}));
        put(_colors, _u, Color::gray());
    };
    for (const auto u : _changedVertices) {
        const auto newDistance = getNewDistance(
            _graph, u, _weight, _distance, _compare, _combine, _inf);
        if (_compare(newDistance.first, get(_distance, u)) < 0) {
            insertInHeap(u, newDistance.first);
        } else if (_compare(newDistance.first, get(_distance, u)) > 0) {
            insertInHeap(u, get(_distance, u));
        }
    }
    const auto adjustHeap = [&](const auto _u, auto _newDistance) {
        const auto& color = get(_colors, _u);
        if (color == Color::gray()) {
            _queue.update(get(_handles, _u), {_newDistance, _u});
        } else {
            insertInHeap(_u, _newDistance);
        }
    };
    const auto relaxNeighbors = [&](typename Graph::vertex_descriptor _v) {
        const auto newDistanceV = getNewDistance(
            _graph, _v, _weight, _distance, _compare, _combine, _inf);
        if (_compare(newDistanceV.first, get(_distance, _v)) < 0) {
            adjustHeap(_v, newDistanceV.first);
        } else if (_compare(newDistanceV.first, get(_distance, _v)) > 0) {
            adjustHeap(_v, get(_distance, _v));
        } else if (get(_colors, _v) == Color::gray()) {
            put(_colors, _v, Color::black());
        }
    };

    while (!_queue.empty()) {
        auto u = _queue.top().second;
        _queue.pop();
        if (get(_colors, u) == Color::black()) {
            continue;
        }
        put(_colors, u, Color::black());
        const auto& [newDistance, newPredecessor, nbEquivalent] =
            computeUpdatedDistance(
                _graph, u, _weight, _distance, _compare, _combine, _inf);
        if (nbEquivalent != 1) {
            return false;
        }
        const auto comparison = _compare(newDistance, get(_distance, u));
        if (comparison < 0) {
            put(_distance, u, newDistance);
            put(_predecessors, u, newPredecessor);
            for (const auto ved :
                boost::make_iterator_range(out_edges(u, _graph))) {
                relaxNeighbors(target(ved, _graph));
            }
        } else if (comparison > 0) {
            put(_distance, u, _inf);
            relaxNeighbors(u);
            for (const auto ved :
                boost::make_iterator_range(out_edges(u, _graph))) {
                relaxNeighbors(target(ved, _graph));
            }
        }
    }
    return true;
}

} // namespace CppRO

#endif
